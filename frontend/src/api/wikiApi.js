export default function WikiApi() {
    this.api = 'https://www.wikidata.org/w/api.php';
    this.sparql_url = 'https://query.wikidata.org/sparql';
    this.sparql_query = function( text, cb, cb_fail ) {
        var url = this.sparql_url + "?format=json&query=" + encodeURIComponent(text) ;
        fetch( url )
            .then( ( res ) => res.json() )
            .catch( cb_fail  )
            .then( ( d ) => cb ( d ) );
    }
    this.sparql_items = function( text, cb, cb_fail ) {
        var self = this ;
        if ( typeof cb_fail == 'undefined' ) {
            cb_fail = function () {
                console.log( "Query has failed :(" ) ;
            }
        }
        
        self.sparql_query( text, function ( d ) {
			if ( typeof d == 'undefined' ) {
				cb_fail() ;
				return ;
			}
            var data = [] ;
            var vars = d.head.vars[0];
            d.results.bindings.forEach(element => {
                const d = element[vars];
                if ( d === "" || typeof d == 'undefined' ) return ;
                if ( d.type !== 'uri' ) return ;
                const item = d.value.split('/').slice(-1)[0]
                if ( vars === "lexeme" ) data.push( item )
                if ( vars === "sense" ) data.push( item.split('-')[0] )
            });
            cb( data );
        }, cb_fail);
    }
}