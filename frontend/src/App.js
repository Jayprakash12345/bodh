import './App.css';
import React, { useState, useEffect } from 'react';
import DataTable from './components/DataTable';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import TabPanel from './components/TabPanel';

import WikiApi from './api/wikiApi';

function a11yProps(index) {
	return {
		id: `vertical-tab-${index}`,
		'aria-controls': `vertical-tabpanel-${index}`,
	};
}

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper,
		display: 'flex',
		height: 200,
	},
	tabs: {
		borderRight: `1px solid ${theme.palette.divider}`,
	},
}));

const wdApi = new WikiApi();

function App() {
	const classes = useStyles();
	const [currentTab, setCurrentTab] = useState(0);
	const [currentUser, setCurrentUser] = useState( null );
	const [sp_text, setSpText] = useState(`SELECT ?lexeme ?lemma ?sense ?sensedef {
	?lexeme wikibase:lemma ?lemma ; dct:language wd:Q9610 ; ontolex:sense ?sense.
	?sense skos:definition ?sensedef. FILTER(?sensedef = "শিব"@bn).
}`); // SPARQL text
	const [ml_text, setMlText] = useState(''); // Manual text
	const [LexItems, setLexItems] = useState([]);

	const handleTabChange = (e, newValue) => {
		setCurrentTab(newValue);
	};

	const handleSubmit = () => {
		if (currentTab === 0) {
			if (sp_text === '') alert("SPARQL query text can't be empty!")
			wdApi.sparql_items(sp_text, function (i) {
				setLexItems( i );
			}, function () {
				alert("Query failed :(")
			});
		} else if (currentTab === 1) {
			if (ml_text === '') alert("Lexeme list text can't be empty!")
			const a = ml_text.split('\n').map(function (i) {
				if( i.trim() !== ""){
					return i.trim();
				}
				return null
			}).filter(Boolean)
			setLexItems(a);
		}
	};

	useEffect( () => {
		fetch('/api/profile')
            .then(response => response.json())
            .then(data => { 
                if( data.logged ){
					setCurrentUser(data.username)
				}
            })
	}, [])
	return (
		<>
			<AppBar position="static">
				<Toolbar>
					<Typography variant="h6">bodh</Typography>
					<div style={{ marginLeft: 'auto'}}>
					{ currentUser ?
						<>
						<Typography style={{ display: "inline"}}>Logged in as {currentUser}   </Typography>
						<Button variant="contained" color="secondary"  style={{backgroundColor: 'orange'}}>
							<a href="/logout" style={{color: 'white'}}>Logout</a>
						</Button>
						</>
						:
						<Button variant="contained" color="secondary"  style={{backgroundColor: 'orange'}}>
							<a href="/login" style={{color: 'white'}}>Login</a>
						</Button>
					}
					</div>
				</Toolbar>
			</AppBar>
			<div className="App" style={{ padding: 30 }}>
				<div className={classes.root}>
					<Tabs orientation="vertical" value={currentTab} onChange={handleTabChange} aria-label="Vertical tabs example" className={classes.tabs}>
						<Tab label="SPARQL query" {...a11yProps(0)} />
						<Tab label="Manual lexeme list"  {...a11yProps(1)} />
					</Tabs>
					<TabPanel value={currentTab} index={0}>
						<TextField
							style={{ padding: 0, width: 800 }}
							multiline rows={8}
							rowsMax={8}
							variant="outlined"
							value={sp_text}
							placeholder="Enter the SPARQL query here"
							onChange={(e) => setSpText(e.target.value)}
						/>
					</TabPanel>
					<TabPanel value={currentTab} index={1}>
						<TextField
							style={{ padding: 0, width: 800 }}
							multiline rows={8}
							rowsMax={8}
							variant="outlined"
							value={ml_text}
							placeholder="Enter the lexemes list"
							onChange={(e) => setMlText(e.target.value)}
						/>
					</TabPanel>
				</div>
				<Button variant="contained" color="primary" onClick={handleSubmit}>Start</Button>
				<br />
				<br />
				<DataTable items={LexItems} />
			</div>
		</>
	);
}

export default App;
