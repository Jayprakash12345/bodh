import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';

// Component import
import Row from './Rows';

class DataTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            data: null
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState( { ...this.state, items: newProps.items });
    }

    componentDidUpdate(prev) {
        if( this.state.items.length > 0 && prev.items !== this.state.items ){
            fetch('https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&origin=*&ids=' + this.state.items.join('|') )
                .then(res => res.ok ? res.json() : Promise.reject())
                .then((d) => this.setState( { ...this.state, data: Object.values( d.entities) } ) )
        }
    }

    render() {
        return (
            <>
            { this.state.items.length > 0 ?
                <Table bordered hover size="sm" style={{ fontSize: '12px'}}>
                    <thead>
                        <tr>
                            <th >#</th>
                            <th>Lexeme</th>
                            <th>Form</th>
                            <th>Sense</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data ? this.state.data.map((item, index) => (
                            <Row index={index} item={item} />
                        )) : 'Loading'}
                    </tbody>
                </Table>
                : null
            }
            </>
        )
    }
}

export default DataTable;