import React, { useState, useEffect } from 'react';

function FormsCell( { data } ) {
    const [ rowData, setRowData ] = useState( data );

    //const [ addNew, setaddNew ] = useState(false)
    const [ newItemLang, setNewItemLang ] = useState('');
    const [ newItemValue, setNewItemValue ] = useState('');

    const handleNewItem = ( e ) => {
        if( e.key === 'Enter'){
            e.preventDefault()

            fetch('/api/createform', {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    lexemeId: rowData[0]["id"].split("-")[0],
                    lang: newItemLang,
                    value: newItemValue
                })
            })
            .then(response => response.json())
            .then(data => { 
                if( data["success"] ){
                    const dataFromResp = {
                        id: data["form"]["id"],
                        representations: {
                            newItemLang: {
                                language: data["form"]["representations"][Object.keys(data["form"]["representations"])[0]]["language"],
                                value: data["form"]["representations"][Object.keys(data["form"]["representations"])[0]]["value"]
                            }
                        },
                        claims: [],
                        grammaticalFeatures: []
                    }

                    setRowData( [...rowData, dataFromResp ] );

                    // Reset the data in new fields
                    setNewItemLang('')
                    setNewItemValue('')
                }
            })
        }
    }

    return (
        <>
            {rowData.map( function(item, index) {
                return([
                    <tr key={index}>
                        <td>{item.id}</td>
                        <td>
                            {item.representations[Object.keys(item.representations)[0]].language}
                        </td>
                        <td>
                            {item.representations[Object.keys(item.representations)[0]].value}
                        </td>
                    </tr>
                ])
            })}
            { //addNew ? 
            <tr key="new">
                <td></td>
                <td><input type="text" placeholder="Lang" size={5} value={newItemLang} onChange={ (e) => setNewItemLang(e.target.value)} /></td>
                <td><input type="text" placeholder="Value" value={newItemValue} onChange={ (e) => setNewItemValue(e.target.value)} onKeyPress={handleNewItem} /></td>
            </tr>
            //: <span style={{ color: 'blue', textDecoration:'underline'}} onClick={() => setaddNew(true)}>New</span>
            }
        </>
    );
}

export default FormsCell;