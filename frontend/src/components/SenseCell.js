import React, { useState } from 'react';

function SenseCell( { data } ) {
    const [ rowData, setRowData ] = useState( data );

    //const [ addNew, setaddNew ] = useState(false)
    const [ newItemLang, setNewItemLang ] = useState('');
    const [ newItemValue, setNewItemValue ] = useState('');

    const handleNewSense = ( e ) => {
        if( e.key === 'Enter'){
            e.preventDefault()
            
            fetch('/api/createsense', {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    lexemeId: rowData[0]["id"].split("-")[0],
                    lang: newItemLang,
                    value: newItemValue
                })
            })
            .then(response => response.json())
            .then(data => {
                if( data["success"] ){
                    const dataFromResp = {
                        id: data["sense"]["id"],
                        glosses: {
                            newItemLang: {
                                language: data["sense"]["glosses"][Object.keys(data["sense"]["glosses"])[0]]["language"],
                                value: data["sense"]["glosses"][Object.keys(data["sense"]["glosses"])[0]]["value"]
                            }
                        },
                        claims: []
                    }
                    setRowData( [...rowData, dataFromResp ] );

                    // Reset the data in new fields
                    setNewItemLang('')
                    setNewItemValue('')
                }
            })
        }
    }

    return (
        <>
            {rowData.map( function(item, index) {
                return([
                    <tr key={index}>
                        <td>{item.id}</td>
                        <td>
                            {item.glosses[Object.keys(item.glosses)[0]].language}
                        </td>
                        <td>
                            {item.glosses[Object.keys(item.glosses)[0]].value}
                        </td>
                    </tr>
                ])
            })}
            { //addNew ? 
            <tr key="new">
                <td></td>
                <td><input type="text" placeholder="Lang" size={5} value={newItemLang} onChange={ (e) => setNewItemLang(e.target.value)} /></td>
                <td><input type="text" placeholder="Value" value={newItemValue} onChange={ (e) => setNewItemValue(e.target.value)} onKeyPress={handleNewSense} /></td>
            </tr>
            //: <span style={{ color: 'blue', textDecoration:'underline'}} onClick={() => setaddNew(true)}>New</span>
            }
        </>
    );
}

export default SenseCell;