import React from 'react';

import Table from 'react-bootstrap/Table';

import FormsCell from './FormsCell';
import SenseCell from './SenseCell';

function Row(props) {
    const { index, item } = props;

    return (
        <tr key={index}>
            <td>
                {index + 1}
            </td>
            <td>
                <a 
                    href={"https://www.wikidata.org/wiki/Lexeme:" + item.id}
                >{item.lemmas[Object.keys(item.lemmas)[0]].value}
                </a>
                <br />
                <span className="lexemeId">({item.id})</span>
            </td>
            <td>
                <Table>
                    <FormsCell data={item.forms} />
                </Table>
            </td>
            <td>
                <Table>
                    <SenseCell data={item.senses} />
                </Table>
            </td>
        </tr>
    );
}

export default Row;