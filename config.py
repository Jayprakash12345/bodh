import os
basedir = os.path.abspath(os.path.dirname(__file__))


class config(object):
    DEBUG = False
    TESTING = False


class production(config):
    DEBUG = False
    CONSUMER_KEY = ""
    CONSUMER_SECRET = ""
    OAUTH_MWURI = "https://www.wikidata.org/w/"
    SESSION_COOKIE_SECURE = True
    SESSION_REFRESH_EACH_REQUEST = False
    PREFERRED_URL_SCHEME = 'https'


class local(config):
    DEVELOPMENT = True
    DEBUG = True
    CONSUMER_KEY = "fe119f488e4b1ab8d3023327a2cba4ab"
    CONSUMER_SECRET = "dd293fa4a5f9a330519eeb97cb2a4f63a2ff869f"
    OAUTH_MWURI = "https://test.wikidata.org/w/"