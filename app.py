from flask import Flask, render_template, jsonify, request, redirect, session, url_for
import json
import os
import requests
from flask_cors import CORS
import mwoauth
from requests_oauthlib import OAuth1

from utils import _str

app = Flask(
    __name__, 
	template_folder='frontend/build',
	static_folder='frontend/build/static'
)
CORS(app)


app.config.from_object( os.environ['APP_SETTINGS'] )
app.secret_key = os.urandom(50)
consumer_token = mwoauth.ConsumerToken(
    app.config["CONSUMER_KEY"],
    app.config["CONSUMER_SECRET"]
)
handshaker = mwoauth.Handshaker(app.config["OAUTH_MWURI"], consumer_token)
API_URL = app.config["OAUTH_MWURI"] + "api.php"


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    print( get_current_user() )
    return render_template('index.html')


@app.route('/api/createform', methods=['POST'])
def createform():
    print(request.get_json())
    lexemeId = request.get_json().get('lexemeId')
    language = request.get_json().get('lang')
    langValue = request.get_json().get('value')
    if(request.method == "POST"):

        # Creating auth session
        ses = authenticated_session()
        #if None not in (old_filename, new_filename, ses):
        csrf_param = {
            "action": "query",
            "meta": "tokens",
            "format": "json"
        }
        response = requests.get(url=API_URL, params=csrf_param, auth=ses)
        data = response.json()
        csrf_token = data["query"]["tokens"]["csrftoken"]
        print( csrf_token )
        param = {
            "action": "wbladdform",
            "format": "json",
            "lexemeId": lexemeId,
            "data": json.dumps({
                "representations": {
                    language: {
                        "language": language,
                        "value": langValue
                    }
                },
                "grammaticalFeatures": [],
                "claims": []
            }),
            "token": csrf_token
        }
        r = requests.post(url=API_URL, data=param, auth=ses)
        #print(jsonify(r.text))
        return json.loads( r.text )

@app.route('/api/createsense', methods=['POST'])
def createsense():
    print(request.get_json())
    lexemeId = request.get_json().get('lexemeId')
    language = request.get_json().get('lang')
    langValue = request.get_json().get('value')
    if(request.method == "POST"):

        # Creating auth session
        ses = authenticated_session()
        #if None not in (old_filename, new_filename, ses):
        csrf_param = {
            "action": "query",
            "meta": "tokens",
            "format": "json"
        }
        response = requests.get(url=API_URL, params=csrf_param, auth=ses)
        data = response.json()
        csrf_token = data["query"]["tokens"]["csrftoken"]
        print( csrf_token )
        param = {
            "action": "wbladdsense",
            "format": "json",
            "lexemeId": lexemeId,
            "data": json.dumps({
                "glosses": {
                    language: {
                        "language": language,
                        "value": langValue
                    }
                }
            }),
            "token": csrf_token
        }
        r = requests.post(url=API_URL, data=param, auth=ses)
        #print(jsonify(r.text))
        return json.loads( r.text )

'''
@app.route('/api/editform', methods=['POST'])
def editform():
    if(request.method == "POST"):
        param = {
            "action": "wbleditformelements",
            "format": "json",
            "formId": request.form.get('formId'),
            "data": json.dumps({
                "representations": {
                    request.form.get('lang'): {
                        "language": request.form.get('lang'),
                        "value": request.form.get('value')
                    }
                },
                "grammaticalFeatures": [
                    "Q163014"
                ],
                "claims": []
            }),
            "token": "+\\"
        }
        r = requests.post(url=URL, data=param)
        return jsonify(r.text)
'''

# ------ API Part -----------
@app.route('/api/profile')
def api_profile():
    return jsonify( {
        "logged": get_current_user() is not None,
        "username": get_current_user()
    })

# ------------ Login --------

@app.route('/login')
def login():
    redirect_to, request_token = handshaker.initiate()
    keyed_token_name = _str(request_token.key) + '_request_token'
    keyed_next_name = _str(request_token.key) + '_next'
    session[keyed_token_name] = \
        dict(zip(request_token._fields, request_token))
    if 'next' in request.args:
        session[keyed_next_name] = request.args.get('next')
    else:
        session[keyed_next_name] = 'index'
    return redirect(redirect_to)


@app.route('/logout')
def logout():
    session['mwoauth_access_token'] = None
    session['mwoauth_username'] = None
    if 'next' in request.args:
        return redirect(request.args['next'])
    return redirect(url_for('index'))


@app.route('/oauth-callback')
def oauth_callback():
    request_token_key = request.args.get('oauth_token', 'None')
    keyed_token_name = _str(request_token_key) + '_request_token'
    keyed_next_name = _str(request_token_key) + '_next'
    if keyed_token_name not in session:
        err_msg = "OAuth callback failed. Can't find keyed token. Are cookies disabled?"
        return render_template("error.html", msg=err_msg)
    access_token = handshaker.complete(
        mwoauth.RequestToken(**session[keyed_token_name]),
        request.query_string)
    session['mwoauth_access_token'] = \
        dict(zip(access_token._fields, access_token))
    next_url = url_for(session[keyed_next_name])
    del session[keyed_next_name]
    del session[keyed_token_name]
    get_current_user(False)
    return redirect(next_url)


@app.before_request
def force_https():
    if request.headers.get('X-Forwarded-Proto') == 'http':
        return redirect(
            'https://' + request.headers['Host'] +
            request.headers['X-Original-URI'],
            code=301
        )


def get_current_user(cached=True):
    if cached:
        return session.get('mwoauth_username')
    # Get user info
    identity = handshaker.identify(
        mwoauth.AccessToken(**session['mwoauth_access_token']))
    # Store user info in session
    session['mwoauth_username'] = identity['username']
    return session['mwoauth_username']

def authenticated_session():
    if 'mwoauth_access_token' in session:
        auth = OAuth1(
            client_key= app.config["CONSUMER_KEY"],
            client_secret= app.config["CONSUMER_SECRET"],
            resource_owner_key=session['mwoauth_access_token']['key'],
            resource_owner_secret=session['mwoauth_access_token']['secret']
        )
        return auth
    return None

if __name__ == "__main__":
    app.run(debug=True)
