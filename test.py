import requests
import json

URL = "https://test.wikidata.org/w/api.php"
# param = {
# 	"action": "wbladdform",
# 	"format": "json",
# 	"lexemeId": "L1399",
# 	"data": "{\n                    \"representations\": {\n                        \"en\": {\n                            \"language\": \"en\",\n                            \"value\": \"cobra\"\n                        }\n                    },\n                    \"grammaticalFeatures\": [\n                        \"Q163014\"\n                    ],\n                    \"claims\": []\n                }",
# 	"token": "+\"
# }

param = {
	"action": "wbladdform",
	"format": "json",
	"lexemeId": 'L1399',
	"data": json.dumps( {
		"id": "L1399-F3",
		"representations": {
			"en": {
				"language": "en",
				"value": "Cobra Ji"
			}
		},
		"grammaticalFeatures": [
			"Q163014"
		],
		"claims": []
	}),
	"token": "+\\"
}

r = requests.post(url=URL, data=param)
print(r.text)

""""
To edit Form
{
	"action": "wbleditformelements",
	"format": "json",
	"formId": "L1399-F3",
	"data": "{ \"representations\": {\"en\": {\n\t\t\t\t\"language\": \"en\",\n\t\t\t\t\"value\": \"Cobra Ji\"\n\t\t\t}\n\t\t},\n\t\t\"grammaticalFeatures\": [\n\t\t\t\"Q163014\"\n\t\t],\n\t\t\"claims\": []\n\t}",
	"token": "aaecca40ab3b01f81a4701a52b1d3d395fb6d13f+\\"
}
"""